#!/bin/bash
# ------------------------------------------------------------------- #
#   BACKUP de fichier vers un site distant
#   -------------------------------------------------
#
#   Auteur : DA COSTA LEITAO Nicolas
#   Site : http://www.ndacostaleitao.info/
#   Date : 11/08/2017
#
#   Principe :
#   ---------
#   Sauvegarde les backups d'un répertoire vers un autre serveur
#   Permet de purger les anciennes sauvegardes
#   Renseigne ses actions dans un fichier log et poste par mail
#   un rapport de ses actions (activable ou non)
#
# --------------------------------------------------------------------- #
# --------------------------------------------------------------------- #
#   PARAMETRES DU SCRIPT
# --------------------------------------------------------------------- #

EMAILID=
nom_archive=
jour=$(date +%d-%m-%y)
FTPLOG=/var/log/sftp/save_sftp"${nom_archive}"_du_"${jour}".log
server=
user=
port=22
mysftppassword=
FILE=.
path1=/
path2=/home/backup

# --------------------------------------------------------------------- #
#   FIN DES PARAMETRES, ne rien changer ensuite                         #
# --------------------------------------------------------------------- #
#**************************Fonction**************************************
gestion_retour() {
	if [ $1 -eq 0 ]; then
		echo "\033[1;32m ... OK \033[0m"
	else
		echo "*[`date +"%d/%m/%y"-"%H:%M"`] ... ERREUR CODE $? " >> $FTPLOG
		mail  -s "Echec BACKUP_"${nom_archive}"_du_"${jour}"" "$EMAILID" < $FTPLOG
		exit 1
	fi
}
checkEnd() {
	if fgrep "100%" $FTPLOG; then
		echo "file ok" >> $FTPLOG
	elif (( $(fgrep "not found" $FTPLOG | wc -l) ==1 )); then
		echo "*[`date +"%d/%m/%y"-"%H:%M"`] No file today" >> $FTPLOG
	else
		echo -e "*[`date +"%d/%m/%y"-"%H:%M"`] File did not download, send email!" >> $FTPLOG && gestion_retour $?
	fi
}
#**************************debut du script********************************
echo "*[`date +"%d/%m/%y"-"%H:%M"`] Logging into external big data SFTP server" > $FTPLOG 
/usr/bin/expect <<_EOF_ >> $FTPLOG  
spawn sftp -P $port $user@$server 
expect "password :"
send "$mysftppassword\r"
expect "sftp>"
send "cd $path1\r"
sleep 2
expect "sftp>"
send "lcd $path2\r"
sleep 6
expect "sftp>"
set timeout -1
send "reget -pr $FILE\r"
sleep 6
expect "sftp>"
sleep 6
send "bye\r"
_EOF_
checkEnd
echo "" >> $FTPLOG 
echo "" >> $FTPLOG 
echo "*[`date +"%d/%m/%y"-"%H:%M"`] Logging off external big data SFTP server" >> $FTPLOG 
echo "----------------------" >> $FTPLOG 
echo "" >> $FTPLOG 
echo "" >> $FTPLOG 
cd $path2 && find . -name '*.tar.gz' -o -name '*.sql.gz' -o -name '*.md5' -mtime +180 | xargs rm -f && gestion_retour $?
echo "*[`date +"%d/%m/%y"-"%H:%M"`] Fin du nettoyage" >> $FTPLOG
echo "----------------------" >> $FTPLOG
exit 0